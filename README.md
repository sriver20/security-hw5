Commands
--------
Disable ASLR
```
sudo vim /etc/sysct1.conf

kernel.exec-shild = 0
kernel.randomize_va_space = 0
```

GCC commands

```
gcc -ggdb -o <binary file name A> <c file name>
gcc -ggdb -o --static <binary file name B> <c file name>
```

GDB commands
```
gdb <binary file name A or B>
disas main
disas ececve
```
