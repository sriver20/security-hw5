
buffer4:     file format elf64-x86-64


Disassembly of section .init:

00000000004003e0 <_init>:
  4003e0:	48 83 ec 08          	sub    $0x8,%rsp
  4003e4:	48 8b 05 0d 0c 20 00 	mov    0x200c0d(%rip),%rax        # 600ff8 <_DYNAMIC+0x1d0>
  4003eb:	48 85 c0             	test   %rax,%rax
  4003ee:	74 05                	je     4003f5 <_init+0x15>
  4003f0:	e8 3b 00 00 00       	callq  400430 <__gmon_start__@plt>
  4003f5:	48 83 c4 08          	add    $0x8,%rsp
  4003f9:	c3                   	retq   

Disassembly of section .plt:

0000000000400400 <__libc_start_main@plt-0x10>:
  400400:	ff 35 02 0c 20 00    	pushq  0x200c02(%rip)        # 601008 <_GLOBAL_OFFSET_TABLE_+0x8>
  400406:	ff 25 04 0c 20 00    	jmpq   *0x200c04(%rip)        # 601010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40040c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400410 <__libc_start_main@plt>:
  400410:	ff 25 02 0c 20 00    	jmpq   *0x200c02(%rip)        # 601018 <_GLOBAL_OFFSET_TABLE_+0x18>
  400416:	68 00 00 00 00       	pushq  $0x0
  40041b:	e9 e0 ff ff ff       	jmpq   400400 <_init+0x20>

0000000000400420 <execve@plt>:
  400420:	ff 25 fa 0b 20 00    	jmpq   *0x200bfa(%rip)        # 601020 <_GLOBAL_OFFSET_TABLE_+0x20>
  400426:	68 01 00 00 00       	pushq  $0x1
  40042b:	e9 d0 ff ff ff       	jmpq   400400 <_init+0x20>

0000000000400430 <__gmon_start__@plt>:
  400430:	ff 25 f2 0b 20 00    	jmpq   *0x200bf2(%rip)        # 601028 <_GLOBAL_OFFSET_TABLE_+0x28>
  400436:	68 02 00 00 00       	pushq  $0x2
  40043b:	e9 c0 ff ff ff       	jmpq   400400 <_init+0x20>

Disassembly of section .text:

0000000000400440 <_start>:
  400440:	31 ed                	xor    %ebp,%ebp
  400442:	49 89 d1             	mov    %rdx,%r9
  400445:	5e                   	pop    %rsi
  400446:	48 89 e2             	mov    %rsp,%rdx
  400449:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  40044d:	50                   	push   %rax
  40044e:	54                   	push   %rsp
  40044f:	49 c7 c0 a0 05 40 00 	mov    $0x4005a0,%r8
  400456:	48 c7 c1 30 05 40 00 	mov    $0x400530,%rcx
  40045d:	48 c7 c7 a2 05 40 00 	mov    $0x4005a2,%rdi
  400464:	e8 a7 ff ff ff       	callq  400410 <__libc_start_main@plt>
  400469:	f4                   	hlt    
  40046a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000400470 <deregister_tm_clones>:
  400470:	b8 47 10 60 00       	mov    $0x601047,%eax
  400475:	55                   	push   %rbp
  400476:	48 2d 40 10 60 00    	sub    $0x601040,%rax
  40047c:	48 83 f8 0e          	cmp    $0xe,%rax
  400480:	48 89 e5             	mov    %rsp,%rbp
  400483:	77 02                	ja     400487 <deregister_tm_clones+0x17>
  400485:	5d                   	pop    %rbp
  400486:	c3                   	retq   
  400487:	b8 00 00 00 00       	mov    $0x0,%eax
  40048c:	48 85 c0             	test   %rax,%rax
  40048f:	74 f4                	je     400485 <deregister_tm_clones+0x15>
  400491:	5d                   	pop    %rbp
  400492:	bf 40 10 60 00       	mov    $0x601040,%edi
  400497:	ff e0                	jmpq   *%rax
  400499:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000004004a0 <register_tm_clones>:
  4004a0:	b8 40 10 60 00       	mov    $0x601040,%eax
  4004a5:	55                   	push   %rbp
  4004a6:	48 2d 40 10 60 00    	sub    $0x601040,%rax
  4004ac:	48 c1 f8 03          	sar    $0x3,%rax
  4004b0:	48 89 e5             	mov    %rsp,%rbp
  4004b3:	48 89 c2             	mov    %rax,%rdx
  4004b6:	48 c1 ea 3f          	shr    $0x3f,%rdx
  4004ba:	48 01 d0             	add    %rdx,%rax
  4004bd:	48 d1 f8             	sar    %rax
  4004c0:	75 02                	jne    4004c4 <register_tm_clones+0x24>
  4004c2:	5d                   	pop    %rbp
  4004c3:	c3                   	retq   
  4004c4:	ba 00 00 00 00       	mov    $0x0,%edx
  4004c9:	48 85 d2             	test   %rdx,%rdx
  4004cc:	74 f4                	je     4004c2 <register_tm_clones+0x22>
  4004ce:	5d                   	pop    %rbp
  4004cf:	48 89 c6             	mov    %rax,%rsi
  4004d2:	bf 40 10 60 00       	mov    $0x601040,%edi
  4004d7:	ff e2                	jmpq   *%rdx
  4004d9:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)

00000000004004e0 <__do_global_dtors_aux>:
  4004e0:	80 3d 59 0b 20 00 00 	cmpb   $0x0,0x200b59(%rip)        # 601040 <__TMC_END__>
  4004e7:	75 11                	jne    4004fa <__do_global_dtors_aux+0x1a>
  4004e9:	55                   	push   %rbp
  4004ea:	48 89 e5             	mov    %rsp,%rbp
  4004ed:	e8 7e ff ff ff       	callq  400470 <deregister_tm_clones>
  4004f2:	5d                   	pop    %rbp
  4004f3:	c6 05 46 0b 20 00 01 	movb   $0x1,0x200b46(%rip)        # 601040 <__TMC_END__>
  4004fa:	f3 c3                	repz retq 
  4004fc:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000400500 <frame_dummy>:
  400500:	48 83 3d 18 09 20 00 	cmpq   $0x0,0x200918(%rip)        # 600e20 <__JCR_END__>
  400507:	00 
  400508:	74 1e                	je     400528 <frame_dummy+0x28>
  40050a:	b8 00 00 00 00       	mov    $0x0,%eax
  40050f:	48 85 c0             	test   %rax,%rax
  400512:	74 14                	je     400528 <frame_dummy+0x28>
  400514:	55                   	push   %rbp
  400515:	bf 20 0e 60 00       	mov    $0x600e20,%edi
  40051a:	48 89 e5             	mov    %rsp,%rbp
  40051d:	ff d0                	callq  *%rax
  40051f:	5d                   	pop    %rbp
  400520:	e9 7b ff ff ff       	jmpq   4004a0 <register_tm_clones>
  400525:	0f 1f 00             	nopl   (%rax)
  400528:	e9 73 ff ff ff       	jmpq   4004a0 <register_tm_clones>
  40052d:	0f 1f 00             	nopl   (%rax)

0000000000400530 <__libc_csu_init>:
  400530:	41 57                	push   %r15
  400532:	41 89 ff             	mov    %edi,%r15d
  400535:	41 56                	push   %r14
  400537:	49 89 f6             	mov    %rsi,%r14
  40053a:	41 55                	push   %r13
  40053c:	49 89 d5             	mov    %rdx,%r13
  40053f:	41 54                	push   %r12
  400541:	4c 8d 25 c8 08 20 00 	lea    0x2008c8(%rip),%r12        # 600e10 <__frame_dummy_init_array_entry>
  400548:	55                   	push   %rbp
  400549:	48 8d 2d c8 08 20 00 	lea    0x2008c8(%rip),%rbp        # 600e18 <__init_array_end>
  400550:	53                   	push   %rbx
  400551:	4c 29 e5             	sub    %r12,%rbp
  400554:	31 db                	xor    %ebx,%ebx
  400556:	48 c1 fd 03          	sar    $0x3,%rbp
  40055a:	48 83 ec 08          	sub    $0x8,%rsp
  40055e:	e8 7d fe ff ff       	callq  4003e0 <_init>
  400563:	48 85 ed             	test   %rbp,%rbp
  400566:	74 1e                	je     400586 <__libc_csu_init+0x56>
  400568:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
  40056f:	00 
  400570:	4c 89 ea             	mov    %r13,%rdx
  400573:	4c 89 f6             	mov    %r14,%rsi
  400576:	44 89 ff             	mov    %r15d,%edi
  400579:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
  40057d:	48 83 c3 01          	add    $0x1,%rbx
  400581:	48 39 eb             	cmp    %rbp,%rbx
  400584:	75 ea                	jne    400570 <__libc_csu_init+0x40>
  400586:	48 83 c4 08          	add    $0x8,%rsp
  40058a:	5b                   	pop    %rbx
  40058b:	5d                   	pop    %rbp
  40058c:	41 5c                	pop    %r12
  40058e:	41 5d                	pop    %r13
  400590:	41 5e                	pop    %r14
  400592:	41 5f                	pop    %r15
  400594:	c3                   	retq   
  400595:	66 66 2e 0f 1f 84 00 	data32 nopw %cs:0x0(%rax,%rax,1)
  40059c:	00 00 00 00 

00000000004005a0 <__libc_csu_fini>:
  4005a0:	f3 c3                	repz retq 

00000000004005a2 <main>:
  4005a2:	55                   	push   %rbp  ; push address of current stack on base pointer
  4005a3:	48 89 e5             	mov    %rsp,%rbp  ; move stack pointer to stack pointer to base pointer
  4005a6:	48 83 ec 30          	sub    $0x30,%rsp ; subtract 48 base 10 rom top of stack
  4005aa:	89 7d dc             	mov    %edi,-0x24(%rbp)
  4005ad:	48 89 75 d0          	mov    %rsi,-0x30(%rbp)
  4005b1:	48 c7 45 e8 f4 05 40 	movq   $0x4005f4,-0x18(%rbp)
  4005b8:	00 
  4005b9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  4005bd:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  4005c1:	48 c7 45 00 00 00 00 	movq   $0x0,0x0(%rbp)
  4005c8:	00 
  4005c9:	48 8d 4d f0          	lea    -0x10(%rbp),%rcx
  4005cd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  4005d1:	ba 00 00 00 00       	mov    $0x0,%edx
  4005d6:	48 89 ce             	mov    %rcx,%rsi
  4005d9:	48 89 c7             	mov    %rax,%rdi
  4005dc:	e8 3f fe ff ff       	callq  400420 <execve@plt>
  4005e1:	c9                   	leaveq 
  4005e2:	c3                   	retq   

Disassembly of section .fini:

00000000004005e4 <_fini>:
  4005e4:	48 83 ec 08          	sub    $0x8,%rsp
  4005e8:	48 83 c4 08          	add    $0x8,%rsp
  4005ec:	c3                   	retq   
