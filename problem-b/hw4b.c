#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void hello( char *tag ) {
    char inp[100];
    printf( "Enter value for %s: ", tag );
    gets( inp );
    printf( "Hello your %s is %s\n", tag, inp );
}

int main(int argc, char *argv[]) {
    char  tag [9]= "Salvador";
    hello(tag);

    return 0;
}

