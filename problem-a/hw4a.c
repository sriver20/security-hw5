#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void hello( char *tag ) {
    char inp[15];
    printf( "Enter value for %s: ", tag );
    // Auto matically cut of input at n - 1.
    // Size value in the middle is max number of
    // charachter including null terminator, n.
    fgets( inp, sizeof(inp), stdin );
    printf( "Hello your %s is %s\n", tag, inp );
}

void display( char *val ) {
    char tmp [25];
    // Auto matically cut of input at n - 1.
    // Size value in the middle is max number of
    // charachter including null terminator, n.
    snprintf(tmp, sizeof(tmp), "Read val: %s\n", val);
    puts(tmp);
}

void echo( char * input ) {
    char output[10];
    output[ sizeof(output) - 1 ] = '\0';
    // Will not pad the null terminator, so you must
    // do it for it.
    strncpy(output, input, sizeof(output) - 1 );
    puts(output);
}

void smtp_mailfrom( char *email_addr ) {
    char msg [ 30 ];
    strncpy(msg, "MAIL FROM: <", sizeof(msg) - 2 );
    // should be minus 1 to leave room for last character.
    strncat(msg, email_addr, sizeof(msg) - sizeof("MAIL FROM: <") - 2);
    strncat(msg, ">", 1);
    puts(msg);

    int x = 0;
}

int append_body ( char *http_req , int http_size, int size_header,
        char *body , int size_body ) {
    int k, j;
    k = size_header ;

    // Leave room for the null terminator
    http_size = http_size - 1;
    http_req[http_size] = '\0';

    if (http_size <= size_header) {
        return http_size;
    }

    if (http_size - 1 == size_header) {
        http_req [ k ] = '\r';
        k++;
        return k;
    }

    if (http_size - 2  == size_header) {
        http_req [ k ] = '\n';
        k++;
        return k;
    }

    http_req [ k ] = '\r';
    k++;
    http_req [ k ] = '\n';
    k++;

    for ( j = 0; j < size_body ; j ++) {
        if (k >= http_size) {
           break;
        }
        http_req[ k ] = body[ j ];
        k++;
    }
    return k ;
}

long int load_file ( char *buf, int buf_size , FILE *fp) {
    long int len;
    buf[buf_size] = '\0';
    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    fread( buf, 1, buf_size - 2 , fp);
    return buf_size;
}

int main(int argc, char *argv[]) {
    // Hello
    printf("Hello\n");
    char  tag [9]= "Salvador";
    hello(tag);
    printf("\n");

    // Display
    printf("Display\n");
    char val [] = "0123456789abcdef";
    display(val);
    printf("\n");

    // Echo
    printf("Echo\n");
    char echoMsg1 [] = "0123456789abcdef0123456789abcdef";
    echo(echoMsg1);
    char  echoMsg2 [9] = "Salvador";
    echo(echoMsg2);
    char  echoMsg3 [9] = "0123";
    echo(echoMsg3);
    printf("\n");

    //SMTP Mail from
    printf("SMTP\n");
    char emailAddress [] = "0123456789abcdef0123456789abcdef";
    //char emailAddress [] = "aaaaaaaaaaaaaaaaaaaa";
    smtp_mailfrom(emailAddress);
    printf("\n");

    //Append Body
    printf("Append Body\n");
    // Change to a smaller value.
    char req[30] = "SamIm";
    int header_size = 5;
    char body[]= "Hello Professor Zhu,\n";
    append_body( req, sizeof(req), header_size, body, sizeof(body) );
    int x = 0;
    while (req[x] != '\0') {
        printf("%c", req[x]);
        x++;
    }
    printf("\n");
    printf("\n");

    //Load File
    printf("Load File\n");
    // Change to a smaller value.
    char buffer[200];
    FILE *fp = fopen("Makefile", "r");
    load_file(buffer, sizeof(buffer), fp);
    fclose(fp);
    x = 0;
    while (buffer[x] != '\0') {
        printf("%c", buffer[x]);
        x++;
    }
    printf("\n");


    return 0;
}



